#include <Adafruit_NeoPixel.h>
#include <TimerOne.h>

#define POWER_PIN             4
#define DATA_PIN              6
#define NUMBER_OF_LEDS        19
#define TIME_PER_ROUND        600000 // in us
#define NUMBER_OF_INTENCITIES 4 
#define MAX_COUNT             ((NUMBER_OF_LEDS)*(NUMBER_OF_INTENCITIES))

// The defines below are just some I wrote for quick testing different colours. Note:
// Only one of these should be true.
#define YELLOW true
#define BLUE false
#define RED false

Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMBER_OF_LEDS, DATA_PIN, NEO_GRB + NEO_KHZ800);

void setup() {
  // Setup sequence. Only power on the ledstrip, when the NeoPixel is ready to turn off all leds. 
  pinMode(POWER_PIN, OUTPUT);
  pixels.begin(); // This initializes the NeoPixel library.
  pixels.clear();

  // I'm not sure this is a good idea, but it seems that the led strip turns off
  // quicker if I start clearing the LED before I have powered it on. 
  pixels.show(); 
  digitalWrite(POWER_PIN, HIGH);
  pixels.show();

  // Initialize timer
  Timer1.initialize(TIME_PER_ROUND/MAX_COUNT); 
  Timer1.attachInterrupt(timerIsr);
}

void loop() {
}

void timerIsr() {
  static uint8_t i=0;
  
  if(++i >= MAX_COUNT) 
    i = 0;
  show(i);
}

void setPixelColour(uint8_t led, uint8_t intencity) {
#if YELLOW
  const uint8_t r = 255, g = 100, b = 0;
#elif BLUE
  const uint8_t r = 10, g = 10, b = 255;
#elif RED
  const uint8_t r = 255, g = 0, b = 0;
#endif

  pixels.setPixelColor(led, pixels.Color(r>>intencity, g>>intencity, b>>intencity));  
}

void show(uint16_t ledNumber) {
  uint8_t currentLed = ledNumber/NUMBER_OF_INTENCITIES;
  uint8_t previousLed = (currentLed-1)%NUMBER_OF_LEDS;
  
  uint8_t previousBrightness = ledNumber%NUMBER_OF_INTENCITIES;  
  uint8_t currentBrightness = NUMBER_OF_INTENCITIES-previousBrightness-1;

  pixels.clear();
  setPixelColour(currentLed, currentBrightness);
  setPixelColour(previousLed, previousBrightness);
  
  pixels.show();
}

